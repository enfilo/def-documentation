.. _fhv-env:

=================
FHV Environment
=================

A DEF instance is maintained at the FHV. It is only possible to connect to this instance within the FHV network. Following the connection data is listed.

================ ============= ======================================================
Component        Name / Id     IP / URL
================ ============= ======================================================
*Manager*        ``manager1``  ``10.0.50.53``
*Web-Manager*                  `http://10.0.50.53 <http://10.0.50.53/>`_
*Cluster*        ``cluster1``  ``10.0.50.55``
*Library*                      via Web-Manager: `http://10.0.50.53/manager/library <http://10.0.50.53/manager/library>`_
================ ============= ======================================================
